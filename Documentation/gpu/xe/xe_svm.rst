.. SPDX-License-Identifier: (GPL-2.0+ OR MIT)

=============
Shared virtual memory
=============

.. kernel-doc:: drivers/gpu/drm/xe/xe_svm_doc.h
   :doc: Shared virtual memory
