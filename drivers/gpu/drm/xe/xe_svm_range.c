// SPDX-License-Identifier: MIT
/*
 * Copyright © 2023 Intel Corporation
 */

#include <linux/interval_tree.h>
#include <linux/mmu_notifier.h>
#include <linux/container_of.h>
#include <linux/mm_types.h>
#include <linux/mutex.h>
#include <linux/mm.h>
#include "xe_svm.h"
#include "xe_pt.h"

/**
 * xe_svm_range_from_addr() - retrieve svm_range contains a virtual address
 *
 * @svm: svm that the virtual address belongs to
 * @addr: the virtual address to retrieve svm_range for
 *
 * return the svm range found,
 * or NULL if no range found
 */
struct xe_svm_range *xe_svm_range_from_addr(struct xe_svm *svm,
									unsigned long addr)
{
	struct interval_tree_node *node;

	mutex_lock(&svm->mutex);
	node = interval_tree_iter_first(&svm->range_tree, addr, addr);
	mutex_unlock(&svm->mutex);
	if (!node)
		return NULL;

	return container_of(node, struct xe_svm_range, inode);
}

/**
 * xe_svm_range_belongs_to_vma() - determine a virtual address range
 * belongs to a vma or not
 *
 * @mm: the mm of the virtual address range
 * @range: the svm virtual address range
 * @vma: the vma to determine the range
 *
 * Returns true if range belongs to vma
 * false otherwise
 */
bool xe_svm_range_belongs_to_vma(struct mm_struct *mm,
								struct xe_svm_range *range,
								struct vm_area_struct *vma)
{
	struct vm_area_struct *vma1, *vma2;
	unsigned long start = range->start;
	unsigned long end = range->end;

	vma1  = find_vma_intersection(mm, start, start + 4);
	vma2  = find_vma_intersection(mm, end - 4, end);

	return (vma1 == vma) && (vma2 == vma);
}

static bool xe_svm_range_invalidate(struct mmu_interval_notifier *mni,
				      const struct mmu_notifier_range *range,
				      unsigned long cur_seq)
{
	struct xe_svm_range *svm_range =
		container_of(mni, struct xe_svm_range, notifier);
	struct xe_svm *svm = svm_range->svm;
	unsigned long length = range->end - range->start;

	/*
	 * MMU_NOTIFY_RELEASE is called upon process exit to notify driver
	 * to release any process resources, such as zap GPU page table
	 * mapping or unregister mmu notifier etc. We already clear GPU
	 * page table  and unregister mmu notifier in in xe_destroy_svm,
	 * upon process exit. So just simply return here.
	 */
	if (range->event == MMU_NOTIFY_RELEASE)
		return true;

	if (mmu_notifier_range_blockable(range))
		mutex_lock(&svm->mutex);
	else if (!mutex_trylock(&svm->mutex))
		return false;

	mmu_interval_set_seq(mni, cur_seq);
	xe_invalidate_svm_range(svm->vm, range->start, length);
	mutex_unlock(&svm->mutex);

	if (range->event == MMU_NOTIFY_UNMAP)
		queue_work(system_unbound_wq, &svm_range->unregister_notifier_work);

	return true;
}

static const struct mmu_interval_notifier_ops xe_svm_mni_ops = {
	.invalidate = xe_svm_range_invalidate,
};

/**
 * unregister a mmu interval notifier for a svm range
 *
 * @range: svm range
 *
 */
void xe_svm_range_unregister_mmu_notifier(struct xe_svm_range *range)
{
	if (!range->mmu_notifier_registered)
		return;

	mmu_interval_notifier_remove(&range->notifier);
	range->mmu_notifier_registered = false;
}

static void xe_svm_unregister_notifier_work(struct work_struct *work)
{
	struct xe_svm_range *range;

	range = container_of(work, struct xe_svm_range, unregister_notifier_work);

	xe_svm_range_unregister_mmu_notifier(range);

	/**
	 * This is called from mmu notifier MUNMAP event. When munmap is called,
	 * this range is not valid any more. Remove it.
	 */
	mutex_lock(&range->svm->mutex);
	interval_tree_remove(&range->inode, &range->svm->range_tree);
	mutex_unlock(&range->svm->mutex);
	kfree(range);
}

/**
 * register a mmu interval notifier to monitor vma change
 *
 * @range: svm range to monitor
 *
 * This has to be called inside a mmap_read_lock
 */
int xe_svm_range_register_mmu_notifier(struct xe_svm_range *range)
{
	struct vm_area_struct *vma = range->vma;
	struct mm_struct *mm = range->svm->mm;
	u64 start, length;
	int ret = 0;

	if (range->mmu_notifier_registered)
		return 0;

	start =  range->start;
	length = range->end - start;
	/** We are inside a mmap_read_lock, but it requires a mmap_write_lock
	 *  to register mmu notifier.
	 */
	mmap_read_unlock(mm);
	mmap_write_lock(mm);
	ret = mmu_interval_notifier_insert_locked(&range->notifier, vma->vm_mm,
						start, length, &xe_svm_mni_ops);
	mmap_write_downgrade(mm);
	if (ret)
		return ret;

	INIT_WORK(&range->unregister_notifier_work, xe_svm_unregister_notifier_work);
	range->mmu_notifier_registered = true;
	return ret;
}

/**
 * xe_svm_range_prepare_destroy() - prepare work to destroy a svm range
 *
 * @range: the svm range to destroy
 *
 * prepare for a svm range destroy: Zap this range from GPU, unregister mmu
 * notifier.
 */
void xe_svm_range_prepare_destroy(struct xe_svm_range *range)
{
	struct xe_vm *vm = range->svm->vm;
	unsigned long length = range->end - range->start;

	xe_invalidate_svm_range(vm, range->start, length);
	xe_svm_range_unregister_mmu_notifier(range);
}

static void add_range_to_svm(struct xe_svm_range *range)
{
	range->inode.start = range->start;
	range->inode.last = range->end;
	mutex_lock(&range->svm->mutex);
	interval_tree_insert(&range->inode, &range->svm->range_tree);
	mutex_unlock(&range->svm->mutex);
}

/**
 * xe_svm_range_create() - create and initialize a svm range
 *
 * @svm: the svm that the range belongs to
 * @vma: the corresponding vma of the range
 *
 * Create range, add it to svm's interval tree. Regiter a mmu
 * interval notifier for this range.
 *
 * Return the pointer of the created svm range
 * or NULL if fail
 */
struct xe_svm_range *xe_svm_range_create(struct xe_svm *svm,
									struct vm_area_struct *vma)
{
	struct xe_svm_range *range = kzalloc(sizeof(*range), GFP_KERNEL);

	if (!range)
		return NULL;

	range->start = vma->vm_start;
	range->end = vma->vm_end;
	range->vma = vma;
	range->svm = svm;

	if (xe_svm_range_register_mmu_notifier(range)){
		kfree(range);
		return NULL;
	}

	add_range_to_svm(range);
	return range;
}
