/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2023 Intel Corporation
 */

#ifndef _XE_SVM_DOC_H_
#define _XE_SVM_DOC_H_

/**
 * DOC: Shared virtual memory
 *
 * Shared Virtual Memory (SVM) allows the programmer to use a single virtual
 * address space shared between threads executing on CPUs and GPUs. It abstracts
 * away from the user the location of the backing memory, and hence simplifies
 * the user programming model. In a non-SVM memory model, user need to explicitly
 * decide memory placement such as device or system memory, also user need to
 * explicitly migrate memory b/t device and system memory.
 *
 * Interface
 * =========
 *
 * SVM makes use of default OS memory allocation and mapping interface such as
 * malloc() and mmap(). The pointer returned from malloc() and mmap() can be
 * directly used on both CPU and GPU program.
 *
 * SVM also provides API to set virtual address range based memory attributes
 * such as preferred memory location, memory migration granularity, and memory
 * atomic attributes etc. This is similar to Linux madvise API.
 *
 * Basic implementation
 * ==============
 *
 * XeKMD implementation is based on Linux kernel Heterogeneous Memory Management
 * (HMM) framework. HMM’s address space mirroring support allows sharing of the
 * address space by duplicating sections of CPU page tables in the device page
 * tables. This enables both CPU and GPU access a physical memory location using
 * the same virtual address.
 *
 * Linux kernel also provides the ability to plugin device memory to the system
 * (as a special ZONE_DEVICE type) and allocates struct page for each device memory
 * page.
 *
 * HMM also provides a mechanism to migrate pages from host to device memory and
 * vice versa.
 *
 * More information on HMM can be found here.
 * https://www.kernel.org/doc/Documentation/vm/hmm.rst
 *
 * Unlike the non-SVM memory allocator (such as gem_create, vm_bind etc), there
 * is no buffer object (BO, such as struct ttm_buffer_object, struct drm_gem_object),
 * in our SVM implementation. We delibrately choose this implementation option
 * to achieve page granularity memory placement, validation, eviction and migration.
 *
 * The SVM layer directly allocate device memory from drm buddy subsystem. The
 * memory is organized as many blocks each of which has 2^n pages. SVM subsystem
 * then mark the usage of each page using a simple bitmap. When all pages in a
 * block are not used anymore, SVM return this block back to drm buddy subsystem.
 *
 * There are 3 events which can trigger SVM subsystem in actions:
 *
 * 1. A mmu notifier callback
 *
 * Since SVM need to mirror the program's CPU virtual address space from GPU side,
 * when program's CPU address space changes, SVM need to make an identical change
 * from GPU side. SVM/hmm use mmu interval notifier to achieve this. SVM register
 * a mmu interval notifier call back function to core mm, and whenever a CPU side
 * virtual address space is changed (i.e., when a virtual address range is unmapped
 * from CPU calling munmap), the registered callback function will be called from
 * core mm. SVM then mirror the CPU address space change from GPU side, i.e., unmap
 * or invalidate the virtual address range from GPU page table.
 *
 * 2. A GPU page fault
 *
 * At the very beginning of a process's life, no virtual address of the process
 * is mapped on GPU page table. So when GPU access any virtual address of the process
 * a GPU page fault is triggered. SVM then decide the best memory location of the
 * fault address (mainly from performance consideration. Some times also consider
 * correctness requirement such as whether GPU can perform atomics operation to
 * certain memory location), migrate memory if necessary, and map the fault address
 * to GPU page table.
 *
 * 3. A CPU page fault
 *
 * A CPU page fault is usually managed by Linux core mm. But in a CPU and GPU
 * mix programming environment, the backing store of a virtual address range
 * can be in GPU's local memory which is not visible to CPU (DEVICE_PRIVATE),
 * so CPU page fault handler need to migrate such pages to system memory for
 * CPU to be able to access them. Such memory migration is device specific.
 * HMM has a callback function (migrate_to_ram function of the dev_pagemap_ops)
 * for device driver to implement.
 *
 *
 * Memory hints: TBD
 * =================
 *
 * Memory eviction: TBD
 * ===============
 *
 * Lock design
 * ===========
 *
 * https://www.kernel.org/doc/Documentation/vm/hmm.rst section "Address space mirroring
 * implemenation and API" described the locking scheme that driver writer has to
 * respect. There are 3 lock mechanism involved in this scheme:
 *
 * 1. Use mmp_read/write_lock to protect VMA, cpu page table operations.  Operation such
 * as munmap/mmap, page table update during numa balance must hold this lock. Hmm_range_fault
 * is a helper function provided by HMM to populate the CPU page table, so it must be called
 * with this lock
 *
 * 2. Use xe_svm::mutex to protect device side page table operation. Any attempt to bind an
 * address range to GPU, or invalidate an address range from GPU, should hold this device lock
 *
 * 3. In the GPU page fault handler, during device page table update, we hold a xe_svm::mutex,
 * but we don't hold the mmap_read/write_lock. So programm's address space can change during
 * the GPU page table update. mmu notifier seq# is used to determine whether unmap happened
 * during during device page table update, if yes, then retry.
 *
 */

#endif
