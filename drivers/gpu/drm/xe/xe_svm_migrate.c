// SPDX-License-Identifier: MIT
/*
 * Copyright © 2023 Intel Corporation
 */

#include <linux/gfp.h>
#include <linux/migrate.h>
#include <linux/dma-mapping.h>
#include <linux/dma-fence.h>
#include <linux/bitops.h>
#include <linux/bitmap.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <drm/drm_buddy.h>
#include "xe_device_types.h"
#include "xe_trace.h"
#include "xe_migrate.h"
#include "xe_ttm_vram_mgr_types.h"
#include "xe_assert.h"
#include "xe_pt.h"
#include "xe_svm.h"


/**
 * alloc_host_page() - allocate one host page for the fault vma
 *
 * @dev: (GPU) device that will access the allocated page
 * @vma: the fault vma that we need allocate page for
 * @addr: the fault address. The allocated page is for this address
 * @dma_addr: used to output the dma address of the allocated page.
 * This dma address will be used for gpu to access this page. GPU
 * access host page through a dma mapped address.
 * @pfn: used to output the pfn of the allocated page.
 *
 * This function allocate one host page for the specified vma. It
 * also does some prepare work for GPU to access this page, such
 * as map this page to iommu (by calling dma_map_page).
 *
 * When this function returns, the page is locked.
 *
 * Return struct page pointer when success
 * NULL otherwise
 */
static struct page *alloc_host_page(struct device *dev,
							 struct vm_area_struct *vma,
							 unsigned long addr,
							 dma_addr_t *dma_addr,
							 unsigned long *pfn)
{
	struct page *page;

	page = alloc_page_vma(GFP_HIGHUSER, vma, addr);
	if (unlikely(!page))
		return NULL;

	/**Lock page per hmm requirement, see hmm.rst*/
	lock_page(page);
	*dma_addr = dma_map_page(dev, page, 0, PAGE_SIZE, DMA_FROM_DEVICE);
	if (unlikely(dma_mapping_error(dev, *dma_addr))) {
		unlock_page(page);
		__free_page(page);
		return NULL;
	}

	*pfn = migrate_pfn(page_to_pfn(page));
	return page;
}

static void free_host_page(struct page *page)
{
	unlock_page(page);
	put_page(page);
}

static inline struct xe_mem_region *page_to_mem_region(struct page *page)
{
	return container_of(page->pgmap, struct xe_mem_region, pagemap);
}

/**
 * migrate_page_vram_to_ram() - migrate one page from vram to ram
 *
 * @vma: The vma that the page is mapped to
 * @addr: The virtual address that the page is mapped to
 * @src_pfn: src page's page frame number
 * @dst_pfn: used to return dstination page (in system ram)'s pfn
 *
 * Allocate one page in system ram and copy memory from device memory
 * to system ram.
 *
 * Return: 0 if this page is already in sram (no need to migrate)
 * 1: successfully migrated this page from vram to sram.
 * error code otherwise
 */
static int migrate_page_vram_to_ram(struct vm_area_struct *vma, unsigned long addr,
						unsigned long src_pfn, unsigned long *dst_pfn)
{
	struct xe_mem_region *mr;
	struct xe_tile *tile;
	struct xe_device *xe;
	struct device *dev;
	dma_addr_t dma_addr = 0;
	struct dma_fence *fence;
	struct page *host_page;
	struct page *src_page;
	u64 src_dpa;

	src_page = migrate_pfn_to_page(src_pfn);
	if (unlikely(!src_page || !(src_pfn & MIGRATE_PFN_MIGRATE)))
		return 0;

	mr = page_to_mem_region(src_page);
	tile = mem_region_to_tile(mr);
	xe = tile_to_xe(tile);
	dev = xe->drm.dev;

	src_dpa = vram_pfn_to_dpa(mr, src_pfn);
	host_page = alloc_host_page(dev, vma, addr, &dma_addr, dst_pfn);
	if (!host_page)
		return -ENOMEM;

	fence = xe_migrate_svm(tile->migrate, src_dpa, true,
						dma_addr, false, PAGE_SIZE);
	if (IS_ERR(fence)) {
		dma_unmap_page(dev, dma_addr, PAGE_SIZE, DMA_FROM_DEVICE);
		free_host_page(host_page);
		return PTR_ERR(fence);
	}

	dma_fence_wait(fence, false);
	dma_fence_put(fence);
	dma_unmap_page(dev, dma_addr, PAGE_SIZE, DMA_FROM_DEVICE);
	return 1;
}

/**
 * xe_devmem_migrate_to_ram() - Migrate memory back to sram on CPU page fault
 *
 * @vmf: cpu vm fault structure, contains fault information such as vma etc.
 *
 * Note, this is in CPU's vm fault handler, caller holds the mmap read lock.
 * FIXME: relook the lock design here. Is there any deadlock?
 *
 * This function migrate one svm range which contains the fault address to sram.
 * We try to maintain a 1:1 mapping b/t the vma and svm_range (i.e., create one
 * svm range for one vma initially and try not to split it). So this scheme end
 * up migrate at the vma granularity. This might not be the best performant scheme
 * when GPU is in the picture.
 *
 * This can be tunned with a migration granularity for  performance, for example,
 * migration 2M for each CPU page fault, or let user specify how much to migrate.
 * But this is more complicated as this scheme requires vma and svm_range splitting.
 *
 * This function should also update GPU page table, so the fault virtual address
 * points to the same sram location from GPU side. This is TBD.
 *
 * Return:
 * 0 on success
 * VM_FAULT_SIGBUS: failed to migrate page to system memory, application
 * will be signaled a SIGBUG
 */
vm_fault_t xe_devm_migrate_to_ram(struct vm_fault *vmf)
{
	struct xe_mem_region *mr = page_to_mem_region(vmf->page);
	struct xe_tile *tile = mem_region_to_tile(mr);
	struct xe_device *xe = tile_to_xe(tile);
	struct vm_area_struct *vma = vmf->vma;
	struct mm_struct *mm = vma->vm_mm;
	struct xe_svm *svm = xe_lookup_svm_by_mm(mm);
	struct xe_svm_range *range = xe_svm_range_from_addr(svm, vmf->address);
	struct xe_vm *vm = svm->vm;
	u64 npages = (range->end - range->start) >> PAGE_SHIFT;
	unsigned long addr = range->start;
	vm_fault_t ret = 0;
	void *buf;
	int i;

	struct migrate_vma migrate_vma = {
		.vma		= vmf->vma,
		.start		= range->start,
		.end		= range->end,
		.pgmap_owner	= xe->drm.dev,
		.flags		= MIGRATE_VMA_SELECT_DEVICE_PRIVATE,
		.fault_page = vmf->page,
	};

	xe_assert(xe, IS_ALIGNED(vmf->address, PAGE_SIZE));
	xe_assert(xe, IS_ALIGNED(range->start, PAGE_SIZE));
	xe_assert(xe, IS_ALIGNED(range->end, PAGE_SIZE));
	/**FIXME: in case of vma split, svm range might not belongs to one vma*/
	xe_assert(xe, xe_svm_range_belongs_to_vma(mm, range, vma));

	buf = kvcalloc(npages, 2* sizeof(*migrate_vma.src), GFP_KERNEL);
	migrate_vma.src = buf;
	migrate_vma.dst = buf + npages;
	if (migrate_vma_setup(&migrate_vma) < 0) {
		ret = VM_FAULT_SIGBUS;
		goto free_buf;
	}

	if (!migrate_vma.cpages)
		goto free_buf;

	trace_xe_svm_migrate_vram_to_sram(range);
	for (i = 0; i < npages; i++) {
		ret = migrate_page_vram_to_ram(vma, addr, migrate_vma.src[i],
							migrate_vma.dst + i);
		if (ret < 0) {
			ret = VM_FAULT_SIGBUS;
			break;
		}

		/** Migration has been successful, unbind src page from gpu,
		 *  and free source page
		 */
		if (ret == 1) {
			struct page *src_page = migrate_pfn_to_page(migrate_vma.src[i]);

			xe_invalidate_svm_range(vm, addr, PAGE_SIZE);
			xe_devm_page_free(src_page);
		}

		addr += PAGE_SIZE;
	}

	migrate_vma_pages(&migrate_vma);
	migrate_vma_finalize(&migrate_vma);
free_buf:
	kvfree(buf);
	return 0;
}


/**
 * svm_migrate_range_to_vram() - migrate backing store of a va range to vram
 * Must be called with mmap_read_lock(mm) held.
 * @range: the va range to migrate. Range should only belong to one vma.
 * @vma: the vma that this range belongs to. @range can cover whole @vma
 * or a sub-range of @vma.
 * @tile: the destination tile which holds the new backing store of the range
 *
 * Returns: negative errno on faiure, 0 on success
 */
int svm_migrate_range_to_vram(struct xe_svm_range *range,
							struct vm_area_struct *vma,
							struct xe_tile *tile)
{
	struct mm_struct *mm = range->svm->mm;
	unsigned long start = range->start;
	unsigned long end = range->end;
	unsigned long npages = (end - start) >> PAGE_SHIFT;
	struct xe_mem_region *mr = &tile->mem.vram;
	struct migrate_vma migrate = {
		.vma		= vma,
		.start		= start,
		.end		= end,
		.pgmap_owner	= tile->xe->drm.dev,
		.flags          = MIGRATE_VMA_SELECT_SYSTEM,
	};
	struct device *dev = tile->xe->drm.dev;
	dma_addr_t *src_dma_addr;
	struct dma_fence *fence;
	struct page *src_page;
	LIST_HEAD(blocks);
	int ret = 0, i;
	u64 dst_dpa;
	void *buf;

	mmap_assert_locked(mm);
	xe_assert(tile->xe, xe_svm_range_belongs_to_vma(mm, range, vma));

	buf = kvcalloc(npages, 2* sizeof(*migrate.src) + sizeof(*src_dma_addr),
					GFP_KERNEL);
	if(!buf)
		return -ENOMEM;
	migrate.src = buf;
	migrate.dst = migrate.src + npages;
	src_dma_addr = (dma_addr_t *) (migrate.dst + npages);
	ret = xe_devm_alloc_pages(tile, npages, &blocks, migrate.dst);
	if (ret)
		goto kfree_buf;

	ret = migrate_vma_setup(&migrate);
	if (ret) {
		drm_err(&tile->xe->drm, "vma setup returned %d for range [%lx - %lx]\n",
				ret, start, end);
		goto free_dst_pages;
	}

	trace_xe_svm_migrate_sram_to_vram(range);
	/**FIXME: partial migration of a range
	 * print a warning for now. If this message
	 * is printed, we need to fall back to page by page
	 * migration: only migrate pages with MIGRATE_PFN_MIGRATE
	 */
	if (migrate.cpages != npages)
		drm_warn(&tile->xe->drm, "Partial migration for range [%lx - %lx], range is %ld pages, migrate only %ld pages\n",
				start, end, npages, migrate.cpages);

	/**Migrate page by page for now.
	 * Both source pages and destination pages can physically not contiguous,
	 * there is no good way to migrate multiple pages per blitter command.
	 */
	for (i = 0; i < npages; i++) {
		src_page = migrate_pfn_to_page(migrate.src[i]);
		if (unlikely(!src_page || !(migrate.src[i] & MIGRATE_PFN_MIGRATE)))
			goto free_dst_page;

		xe_assert(tile->xe, !is_zone_device_page(src_page));
		src_dma_addr[i] = dma_map_page(dev, src_page, 0, PAGE_SIZE, DMA_TO_DEVICE);
		if (unlikely(dma_mapping_error(dev, src_dma_addr[i]))) {
			drm_warn(&tile->xe->drm, "dma map error for host pfn %lx\n", migrate.src[i]);
			goto free_dst_page;
		}
		dst_dpa = vram_pfn_to_dpa(mr, migrate.dst[i]);
		fence = xe_migrate_svm(tile->migrate, src_dma_addr[i], false,
				dst_dpa, true, PAGE_SIZE);
		if (IS_ERR(fence)) {
			drm_warn(&tile->xe->drm, "migrate host page (pfn: %lx) to vram failed\n",
					migrate.src[i]);
			/**Migration is best effort. Even we failed here, we continue*/
			goto free_dst_page;
		}
		/**FIXME: Use the first migration's out fence as the second migration's input fence,
		 * and so on. Only wait the out fence of last migration?
		 */
		dma_fence_wait(fence, false);
		dma_fence_put(fence);
free_dst_page:
		xe_devm_page_free(pfn_to_page(migrate.dst[i]));
	}

	for (i = 0; i < npages; i++)
		if (!(dma_mapping_error(dev, src_dma_addr[i])))
			dma_unmap_page(dev, src_dma_addr[i], PAGE_SIZE, DMA_TO_DEVICE);

	migrate_vma_pages(&migrate);
	migrate_vma_finalize(&migrate);
free_dst_pages:
	if (ret)
		xe_devm_free_blocks(&blocks);
kfree_buf:
	kfree(buf);
	return ret;
}
