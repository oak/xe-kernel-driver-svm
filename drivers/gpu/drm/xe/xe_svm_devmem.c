// SPDX-License-Identifier: MIT
/*
 * Copyright © 2023 Intel Corporation
 */

#include <linux/mm_types.h>
#include <linux/sched/mm.h>
#include <linux/gfp.h>
#include <linux/migrate.h>
#include <linux/dma-mapping.h>
#include <linux/dma-fence.h>
#include <linux/bitops.h>
#include <linux/bitmap.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <drm/drm_buddy.h>
#include "xe_device_types.h"
#include "xe_trace.h"
#include "xe_migrate.h"
#include "xe_ttm_vram_mgr_types.h"
#include "xe_assert.h"
#include "xe_pt.h"
#include "xe_svm.h"

/**
 * struct xe_svm_block_meta - svm uses this data structure to manage each
 * block allocated from drm buddy. This will be set to the drm_buddy_block's
 * private field.
 *
 * @lru: used to link this block to drm's lru lists. This will be replace
 * with struct drm_lru_entity later.
 * @tile: tile from which we allocated this block
 * @bitmap: A bitmap of each page in this block. 1 means this page is used,
 * 0 means this page is idle. When all bits of this block are 0, it is time
 * to return this block to drm buddy subsystem.
 */
struct xe_svm_block_meta {
	struct list_head lru;
	struct xe_tile *tile;
	unsigned long bitmap[];
};

static u64 block_offset_to_pfn(struct xe_mem_region *mr, u64 offset)
{
	/** DRM buddy's block offset is 0-based*/
	offset += mr->hpa_base;

	return PHYS_PFN(offset);
}

/**
 * xe_devm_alloc_pages() - allocate device pages from buddy allocator
 *
 * @xe_tile: which tile to allocate device memory from
 * @npages: how many pages to allocate
 * @blocks: used to return the allocated blocks
 * @pfn: used to return the pfn of all allocated pages. Must be big enough
 * to hold at @npages entries.
 *
 * This function allocate blocks of memory from drm buddy allocator, and
 * performs initialization work: set struct page::zone_device_data to point
 * to the memory block; set/initialize drm_buddy_block::private field;
 * lock_page for each page allocated; add memory block to lru managers lru
 * list - this is TBD.
 *
 * return: 0 on success
 * error code otherwise
 */
int xe_devm_alloc_pages(struct xe_tile *tile,
						unsigned long npages,
						struct list_head *blocks,
						unsigned long *pfn)
{
	struct drm_buddy *mm = &tile->mem.vram_mgr->mm;
	struct drm_buddy_block *block, *tmp;
	u64 size = npages << PAGE_SHIFT;
	int ret = 0, i, j = 0;

	ret = drm_buddy_alloc_blocks(mm, 0, mm->size, size, PAGE_SIZE,
						blocks, DRM_BUDDY_TOPDOWN_ALLOCATION);

	if (unlikely(ret))
		return ret;

	list_for_each_entry_safe(block, tmp, blocks, link) {
		struct xe_mem_region *mr = &tile->mem.vram;
		u64 block_pfn_first, pages_per_block;
		struct xe_svm_block_meta *meta;
		u32 meta_size;

		size = drm_buddy_block_size(mm, block);
		pages_per_block = size >> PAGE_SHIFT;
		meta_size = BITS_TO_BYTES(pages_per_block) +
					sizeof(struct xe_svm_block_meta);
		meta = kzalloc(meta_size, GFP_KERNEL);
		bitmap_fill(meta->bitmap, pages_per_block);
		meta->tile = tile;
		block->private = meta;
		block_pfn_first =
					block_offset_to_pfn(mr, drm_buddy_block_offset(block));
		trace_xe_buddy_block_alloc(block, size, block_pfn_first);
		for(i = 0; i < pages_per_block; i++) {
			struct page *page;

			pfn[j++] = block_pfn_first + i;
			page = pfn_to_page(block_pfn_first + i);
			/**Lock page per hmm requirement, see hmm.rst.*/
			zone_device_page_init(page);
			page->zone_device_data = block;
		}
	}

	return ret;
}

/** FIXME: we locked page by calling zone_device_page_init
 *  inxe_dev_alloc_pages. Should we unlock pages here?
 */
static void free_block(struct drm_buddy_block *block)
{
	struct xe_svm_block_meta *meta =
		(struct xe_svm_block_meta *)block->private;
	struct xe_tile *tile  = meta->tile;
	struct drm_buddy *mm = &tile->mem.vram_mgr->mm;

	kfree(block->private);
	drm_buddy_free_block(mm, block);
}

/**
 * xe_devm_free_blocks() - free all memory blocks
 *
 * @blocks: memory blocks list head
 */
void xe_devm_free_blocks(struct list_head *blocks)
{
	struct drm_buddy_block *block, *tmp;

	list_for_each_entry_safe(block, tmp, blocks, link)
		free_block(block);
}

void xe_devm_page_free(struct page *page)
{
	struct drm_buddy_block *block =
					(struct drm_buddy_block *)page->zone_device_data;
	struct xe_svm_block_meta *meta =
					(struct xe_svm_block_meta *)block->private;
	struct xe_tile *tile  = meta->tile;
	struct xe_mem_region *mr = &tile->mem.vram;
	struct drm_buddy *mm = &tile->mem.vram_mgr->mm;
	u64 size = drm_buddy_block_size(mm, block);
	u64 pages_per_block = size >> PAGE_SHIFT;
	u64 block_pfn_first =
					block_offset_to_pfn(mr, drm_buddy_block_offset(block));
	u64 page_pfn = page_to_pfn(page);
	u64 i = page_pfn - block_pfn_first;

	xe_assert(tile->xe, i < pages_per_block);
	clear_bit(i, meta->bitmap);
	if (bitmap_empty(meta->bitmap, pages_per_block)) {
		free_block(block);
		trace_xe_buddy_block_free(block, size, block_pfn_first);
	}
}

static const struct dev_pagemap_ops xe_devm_pagemap_ops = {
	.page_free = xe_devm_page_free,
	.migrate_to_ram = xe_devm_migrate_to_ram,
};

/**
 * xe_svm_devm_add: Remap and provide memmap backing for device memory
 * @tile: tile that the memory region blongs to
 * @mr: memory region to remap
 *
 * This remap device memory to host physical address space and create
 * struct page to back device memory
 *
 * Return: 0 on success standard error code otherwise
 */
int xe_svm_devm_add(struct xe_tile *tile, struct xe_mem_region *mr)
{
	struct device *dev = &to_pci_dev(tile->xe->drm.dev)->dev;
	struct resource *res;
	void *addr;
	int ret;

	res = devm_request_free_mem_region(dev, &iomem_resource,
					   mr->usable_size);
	if (IS_ERR(res)) {
		ret = PTR_ERR(res);
		return ret;
	}

	mr->pagemap.type = MEMORY_DEVICE_PRIVATE;
	mr->pagemap.range.start = res->start;
	mr->pagemap.range.end = res->end;
	mr->pagemap.nr_range = 1;
	mr->pagemap.ops = &xe_devm_pagemap_ops;
	mr->pagemap.owner = tile->xe->drm.dev;
	addr = devm_memremap_pages(dev, &mr->pagemap);
	if (IS_ERR(addr)) {
		devm_release_mem_region(dev, res->start, resource_size(res));
		ret = PTR_ERR(addr);
		drm_err(&tile->xe->drm, "Failed to remap tile %d memory, errno %d\n",
				tile->id, ret);
		return ret;
	}
	mr->hpa_base = res->start;

	drm_info(&tile->xe->drm, "Added tile %d memory [%llx-%llx] to devm, remapped to %pr\n",
			tile->id, mr->io_start, mr->io_start + mr->usable_size, res);
	return 0;
}

/**
 * xe_svm_devm_remove: Unmap device memory and free resources
 * @xe: xe device
 * @mr: memory region to remove
 */
void xe_svm_devm_remove(struct xe_device *xe, struct xe_mem_region *mr)
{
	struct device *dev = &to_pci_dev(xe->drm.dev)->dev;

	if (mr->hpa_base) {
		devm_memunmap_pages(dev, &mr->pagemap);
		devm_release_mem_region(dev, mr->pagemap.range.start,
			mr->pagemap.range.end - mr->pagemap.range.start +1);
	}
}

