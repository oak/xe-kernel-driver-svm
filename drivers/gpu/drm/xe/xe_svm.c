// SPDX-License-Identifier: MIT
/*
 * Copyright © 2023 Intel Corporation
 */

#include <linux/mutex.h>
#include <linux/mm_types.h>
#include <linux/interval_tree.h>
#include <linux/container_of.h>
#include <linux/types.h>
#include <linux/migrate.h>
#include "xe_svm.h"
#include <linux/hmm.h>
#include <linux/scatterlist.h>
#include <drm/xe_drm.h>
#include "xe_pt.h"
#include "xe_assert.h"
#include "xe_vm_types.h"
#include "xe_gt.h"
#include "xe_migrate.h"
#include "xe_trace.h"

DEFINE_HASHTABLE(xe_svm_table, XE_MAX_SVM_PROCESS);

/**
 * xe_destroy_svm() - destroy a svm process
 *
 * @svm: the xe_svm to destroy
 */
void xe_destroy_svm(struct xe_svm *svm)
{
#define MAX_SVM_RANGE (1024*1024)
	struct xe_svm_range **range_array;
	struct interval_tree_node *node;
	struct xe_svm_range *range;
	int i = 0;

	range_array = kzalloc(sizeof(struct xe_svm_range *) * MAX_SVM_RANGE,
							GFP_KERNEL);
	node = interval_tree_iter_first(&svm->range_tree, 0, ~0ULL);
	while (node) {
		range = container_of(node, struct xe_svm_range, inode);
		xe_svm_range_prepare_destroy(range);
		node = interval_tree_iter_next(node, 0, ~0ULL);
		xe_assert(svm->vm->xe, i < MAX_SVM_RANGE);
		range_array[i++] = range;
	}

	/** Free range (thus range->inode) while traversing above is not safe */
	while(i)
		kfree(range_array[i--]);

	hash_del_rcu(&svm->hnode);
	mutex_destroy(&svm->mutex);
	kfree(svm);
	kfree(range_array);
}

/**
 * xe_create_svm() - create a svm process
 *
 * @vm: the xe_vm that we create svm process for
 *
 * one xe_svm struct represent a shared address space
 * between cpu and gpu program. So one xe_svm is associated
 * to one mm_struct.
 *
 * If xe_svm for this process already exists, just return
 * it; otherwise create one.
 *
 * Return the created xe svm struct
 */
struct xe_svm *xe_create_svm(struct xe_vm *vm)
{
	struct mm_struct *mm = current->mm;
	struct xe_svm *svm;

	svm = xe_lookup_svm_by_mm(mm);
	if (svm)
		return svm;

	svm = kzalloc(sizeof(struct xe_svm), GFP_KERNEL);
	svm->mm = mm;
	svm->vm	= vm;
	svm->range_tree = RB_ROOT_CACHED;
	mutex_init(&svm->mutex);
	/** Add svm to global xe_svm_table hash table
	 *  use mm as key so later we can retrieve svm using mm
	 */
	hash_add_rcu(xe_svm_table, &svm->hnode, (uintptr_t)mm);
	return svm;
}

/**
 * xe_lookup_svm_by_mm() - retrieve xe_svm from mm struct
 *
 * @mm: the mm struct of the svm to retrieve
 *
 * Return the xe_svm struct pointer, or NULL if fail
 */
struct xe_svm *xe_lookup_svm_by_mm(struct mm_struct *mm)
{
	struct xe_svm *svm;

	hash_for_each_possible_rcu(xe_svm_table, svm, hnode, (uintptr_t)mm)
		if (svm->mm == mm)
			return svm;

	return NULL;
}

/**
 * xe_svm_build_sg() - build a scatter gather table for all the physical pages/pfn
 * in a hmm_range.
 *
 * @range: the hmm range that we build the sg table from. range->hmm_pfns[]
 * has the pfn numbers of pages that back up this hmm address range.
 * @st: pointer to the sg table.
 *
 * All the contiguous pfns will be collapsed into one entry in
 * the scatter gather table. This is for the convenience of
 * later on operations to bind address range to GPU page table.
 *
 * This function allocates the storage of the sg table. It is
 * caller's responsibility to free it calling sg_free_table.
 *
 * Returns 0 if successful; -ENOMEM if fails to allocate memory
 */
int xe_svm_build_sg(struct hmm_range *range,
			     struct sg_table *st)
{
	struct scatterlist *sg;
	u64 i, npages;

	sg = NULL;
	st->nents = 0;
	npages = ((range->end - 1) >> PAGE_SHIFT) - (range->start >> PAGE_SHIFT) + 1;

	if (unlikely(sg_alloc_table(st, npages, GFP_KERNEL)))
		return -ENOMEM;

	for (i = 0; i < npages; i++) {
		unsigned long addr = range->hmm_pfns[i];

		if (sg && (addr == (sg_dma_address(sg) + sg->length))) {
			sg->length += PAGE_SIZE;
			sg_dma_len(sg) += PAGE_SIZE;
			continue;
		}

		sg =  sg ? sg_next(sg) : st->sgl;
		sg_dma_address(sg) = addr;
		sg_dma_len(sg) = PAGE_SIZE;
		sg->length = PAGE_SIZE;
		st->nents++;
	}

	sg_mark_end(sg);
	return 0;
}

/** Populate physical pages of a virtual address range
 * This function also read mmu notifier sequence # (
 * mmu_interval_read_begin), for the purpose of later
 * comparison (through mmu_interval_read_retry).
 * This must be called with mmap read or write lock held.
 *
 * This function alloates hmm_range->hmm_pfns, it is caller's
 * responsibility to free it.
 *
 * @svm_range: The svm range to populate
 * @hmm_range: pointer to hmm_range struct. hmm_rang->hmm_pfns
 * will hold the populated pfns.
 * @write: populate pages with write permission
 *
 * returns: 0 for succuss; negative error no on failure
 */
static int svm_populate_range(struct xe_svm_range *svm_range,
			    struct hmm_range *hmm_range, bool write)
{
	unsigned long timeout =
		jiffies + msecs_to_jiffies(HMM_RANGE_DEFAULT_TIMEOUT);
	unsigned long *pfns, flags = HMM_PFN_REQ_FAULT;
	u64 npages;
	int ret;

	mmap_assert_locked(svm_range->svm->mm);

	npages = ((svm_range->end - 1) >> PAGE_SHIFT) -
						(svm_range->start >> PAGE_SHIFT) + 1;
	pfns = kvmalloc_array(npages, sizeof(*pfns), GFP_KERNEL);
	if (unlikely(!pfns))
		return -ENOMEM;

	if (write)
		flags |= HMM_PFN_REQ_WRITE;

	memset64((u64 *)pfns, (u64)flags, npages);
	hmm_range->hmm_pfns = pfns;
	hmm_range->notifier_seq = mmu_interval_read_begin(&svm_range->notifier);
	hmm_range->notifier = &svm_range->notifier;
	hmm_range->start = svm_range->start;
	hmm_range->end = svm_range->end;
	hmm_range->pfn_flags_mask = HMM_PFN_REQ_FAULT | HMM_PFN_REQ_WRITE;
	hmm_range->dev_private_owner = svm_range->svm->vm->xe->drm.dev;

	while (true) {
		ret = hmm_range_fault(hmm_range);
		if (time_after(jiffies, timeout))
			goto free_pfns;

		if (ret == -EBUSY)
			continue;
		break;
	}

free_pfns:
	if (ret)
		kvfree(pfns);
	return ret;
}

/**
 * svm_access_allowed() -  Determine whether read or/and write to vma is allowed
 *
 * @write: true means a read and write access; false: read only access
 */
static bool svm_access_allowed(struct vm_area_struct *vma, bool write)
{
	unsigned long access = VM_READ;

	if (write)
		access |= VM_WRITE;

	return (vma->vm_flags & access) == access;
}

/**
 * svm_should_migrate() - Determine whether we should migrate a range to
 * a destination memory region
 *
 * @range: The svm memory range to consider
 * @dst_region: target destination memory region
 * @is_atomic_fault: Is the intended migration triggered by a atomic access?
 * On some platform, we have to migrate memory to guarantee atomic correctness.
 */
static bool svm_should_migrate(struct xe_svm_range *range,
				struct xe_mem_region *dst_region, bool is_atomic_fault)
{
	return true;
}

/**
 * xe_svm_handle_gpu_fault() - gpu page fault handler for svm subsystem
 *
 * @vm: The vm of the fault.
 * @gt: The gt hardware on which the fault happens.
 * @pf: page fault descriptor
 *
 * Workout a backing memory for the fault address, migrate memory from
 * system memory to gpu vram if nessary, and map the fault address to
 * GPU so GPU HW can retry the last operation which has caused the GPU
 * page fault.
 */
int xe_svm_handle_gpu_fault(struct xe_vm *vm,
				struct xe_gt *gt,
				struct pagefault *pf)
{
	u8 access_type = pf->access_type;
	u64 page_addr = pf->page_addr;
	struct hmm_range hmm_range;
	struct vm_area_struct *vma;
	struct xe_svm_range *range;
	struct mm_struct *mm;
	struct xe_svm *svm;
	int ret = 0;

	svm = vm->svm;
	if (!svm)
		return -EINVAL;

	mm = svm->mm;
	mmap_read_lock(mm);
	vma = find_vma_intersection(mm, page_addr, page_addr + 4);
	if (!vma) {
		mmap_read_unlock(mm);
		return -ENOENT;
	}

	if (!svm_access_allowed (vma, access_type != ACCESS_TYPE_READ)) {
		mmap_read_unlock(mm);
		return -EPERM;
	}

	range = xe_svm_range_from_addr(svm, page_addr);
	if (!range) {
		range = xe_svm_range_create(svm, vma);
		if (!range) {
			mmap_read_unlock(mm);
			return -ENOMEM;
		}
	}

	if (svm_should_migrate(range, &gt->tile->mem.vram,
						access_type == ACCESS_TYPE_ATOMIC))
		/** Migrate whole svm range for now.
		 *  This is subject to change once we introduce a migration granularity
		 *  parameter for user to select.
		 *
		 *	Migration is best effort. If we failed to migrate to vram,
		 *	we just map that range to gpu in system memory. For cases
		 *	such as gpu atomic operation which requires memory to be
		 *	resident in vram, we will fault again and retry migration.
		 */
		svm_migrate_range_to_vram(range, vma, gt->tile);

	ret = svm_populate_range(range, &hmm_range, vma->vm_flags & VM_WRITE);
	mmap_read_unlock(mm);
	/** There is no need to destroy this range. Range can be reused later */
	if (ret)
		goto free_pfns;

	/**FIXME: set the DM, AE flags in PTE*/
	ret = xe_bind_svm_range(vm, gt->tile, &hmm_range,
		!(vma->vm_flags & VM_WRITE) ? DRM_XE_VM_BIND_FLAG_READONLY : 0);
	/** Concurrent cpu page table update happened,
	 *  Return successfully so we will retry everything
	 *  on next gpu page fault.
	 */
	if (ret == -EAGAIN)
		ret = 0;

free_pfns:
	kvfree(hmm_range.hmm_pfns);
	return ret;
}
