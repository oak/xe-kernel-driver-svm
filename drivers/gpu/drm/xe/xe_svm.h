// SPDX-License-Identifier: MIT
/*
 * Copyright © 2023 Intel Corporation
 */

#ifndef __XE_SVM_H
#define __XE_SVM_H
#include <linux/list.h>
#include <linux/mutex.h>
#include <linux/mmu_notifier.h>
#include <linux/workqueue.h>
#include <linux/rbtree_types.h>
#include <linux/interval_tree.h>
#include <linux/hashtable.h>
#include <linux/mm_types.h>
#include <linux/types.h>
#include <linux/hmm.h>
#include <linux/mm.h>
#include "xe_device_types.h"

struct xe_vm;
struct mm_struct;
struct pagefault;

#define XE_MAX_SVM_PROCESS 5 /* Maximumly support 32 SVM process*/
extern DECLARE_HASHTABLE(xe_svm_table, XE_MAX_SVM_PROCESS);

/**
 * struct xe_svm - data structure to represent a shared
 * virtual address space from device side. xe_svm, xe_vm
 * and mm_struct has a 1:1:1 relationship.
 */
struct xe_svm {
	/** @vm: The xe_vm address space corresponding to this xe_svm */
	struct xe_vm *vm;
	/** @mm: The mm_struct corresponding to this xe_svm */
	struct mm_struct *mm;
	/**
	 * @mutex: A lock used by svm subsystem. It protects:
	 * 1. below range_tree
	 * 2. GPU page table update. Serialize all SVM GPU page table updates
	 */
	struct mutex mutex;
	/**
	 * @range_tree: Interval tree of all svm ranges in this svm
	 */
	struct rb_root_cached range_tree;
	/** @hnode: used to add this svm to a global xe_svm_hash table*/
	struct hlist_node hnode;
};

/**
 * struct xe_svm_range - Represents a shared virtual address range.
 */
struct xe_svm_range {
	/** @svm: pointer of the xe_svm that this range belongs to */
	struct xe_svm *svm;
	/** @notifier: The mmu interval notifer used to keep track of CPU
	 * side address range change. Driver will get a callback with this
	 * notifier if anything changed from CPU side, such as range is
	 * unmapped from CPU
	 */
	struct mmu_interval_notifier notifier;
	bool mmu_notifier_registered;
	/** @start: start address of this range, inclusive */
	u64 start;
	/** @end: end address of this range, exclusive */
	u64 end;
	/** @vma: the corresponding vma of this svm range
	 *  The relationship b/t vma and svm range is 1:N,
	 *  which means one vma can be splitted into multiple
	 *  @xe_svm_range while one @xe_svm_range can have
	 *  only one vma. A N:N mapping means some complication
	 *  in codes. Lets assume 1:N for now.
	 */
	struct vm_area_struct *vma;
	/** @unregister_notifier_work: A worker used to unregister this notifier */
	struct work_struct unregister_notifier_work;
	/** @inode: used to link this range to svm's range_tree */
	struct interval_tree_node inode;
};

vm_fault_t xe_devm_migrate_to_ram(struct vm_fault *vmf);
int svm_migrate_range_to_vram(struct xe_svm_range *range,
							struct vm_area_struct *vma,
							struct xe_tile *tile);
void xe_destroy_svm(struct xe_svm *svm);
struct xe_svm *xe_create_svm(struct xe_vm *vm);
struct xe_svm *xe_lookup_svm_by_mm(struct mm_struct *mm);
struct xe_svm_range *xe_svm_range_from_addr(struct xe_svm *svm,
								unsigned long addr);
bool xe_svm_range_belongs_to_vma(struct mm_struct *mm,
								struct xe_svm_range *range,
								struct vm_area_struct *vma);
void xe_svm_range_unregister_mmu_notifier(struct xe_svm_range *range);
int xe_svm_range_register_mmu_notifier(struct xe_svm_range *range);
void xe_svm_range_prepare_destroy(struct xe_svm_range *range);
struct xe_svm_range *xe_svm_range_create(struct xe_svm *svm,
									struct vm_area_struct *vma);

int xe_svm_build_sg(struct hmm_range *range, struct sg_table *st);
int xe_svm_devm_add(struct xe_tile *tile, struct xe_mem_region *mem);
void xe_svm_devm_remove(struct xe_device *xe, struct xe_mem_region *mem);

int xe_devm_alloc_pages(struct xe_tile *tile,
						unsigned long npages,
						struct list_head *blocks,
						unsigned long *pfn);

void xe_devm_free_blocks(struct list_head *blocks);
void xe_devm_page_free(struct page *page);
int xe_svm_handle_gpu_fault(struct xe_vm *vm,
				struct xe_gt *gt,
				struct pagefault *pf);
#endif
